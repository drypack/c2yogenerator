(function() {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /**
   * Arquivo de configuração com as rotas específicas do recurso <%= resource_name %>
   *
   * @param {object} $stateProvider
   * @param {object} Global
   */
  /** @ngInject */
  function routes($stateProvider, Global) {
    $stateProvider
      .state('app.<%= resource_name %>', {
        url: '/<%= resource_name %>',
        templateUrl: Global.clientPath + '/<%= resource_name %>/<%= resource_name %>.html',
        controller: '<%= name_uppercase %>Controller as <%= alias_controller %>',
        data: { }
      });
  }
}());
