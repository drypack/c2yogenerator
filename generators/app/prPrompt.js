'use strict';

var chalk = require('chalk');
var utils = require('./utils');

/**
 *
 * Monta as questões para serem exibidas no console
 * @return {Object} - Retorna um objeto contendo as questões
 */
function mountQuestions() {
  var questions = {};

  questions.structure = {
    type: 'list',
    name: 'structure',
    message: chalk.yellow('Choose what you want:'),
    choices: [
      { name: chalk.yellow('Complete structure for component.'), value: 'complete' },
      { name: chalk.yellow('Only html.'), value: 'html' },
      { name: chalk.yellow('Only controller.'), value: 'controller' },
      { name: chalk.yellow('Only service.'), value: 'service' },
      { name: chalk.yellow('Only route.'), value: 'route' },
      { name: chalk.yellow('Exit.'), value: 'exit' }
    ]
  };
  questions.name = {
    type: 'input',
    name: 'resourceName',
    message: chalk.yellow('Type the resource name (lowercase and no special char): '),
    validate: function (value) {
      return utils.verifyInput(value);
    }
  };
  questions.htmlConfirm = {
    type: 'list',
    name: 'htmlController',
    message: chalk.yellow('Do you want to generate the correspondents html files?'),
    choices: [{ name: 'Sim', value: true }, { name: 'Não', value: false }]
  };

  return questions;

}

module.exports = {
  mountQuestions: mountQuestions
}
