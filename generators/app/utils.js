'use strict';

var path = require('path');
var slash = require('slash');
var chalk = require('chalk');
var Promise = require('promise');

/**
 * Turn str into simplest form, remove trailing slash
 * example:
 * './path/to//some/folder/' (unix) will be normalized to 'path/to/some/folder'
 * 'path\\to\\some\\folder\\' (windows) will be normalized to 'path/to/some/folder'
 * @param  {String} str, can be unix style path ('/') or windows style path ('\\')
 * @return {String} normalized unix style path
 */
function normalizePath(path, str) {
  var trailingSlash;
  if (path.sep === '/') {
    trailingSlash = new RegExp(path.sep + '$');
  } else {
    trailingSlash = new RegExp(path.sep + path.sep + '$');
  }
  return slash(path.normalize(str).replace(trailingSlash, ''));
}

/**
 * Formata o nome do recurso para os parâmetros esperados
 * nos templates
 * @param {String} resource, Nome do recurso a ser formatado
 * @return {Object} retorna um objeto de contexto para ser usado nos templates
 */
function formatNameResource(resource) {

  var resourceNameLowerCase = resource.toLowerCase();
  var char = resourceNameLowerCase.charAt(0).toUpperCase();
  var nameFistCharUpperCase = char + resourceNameLowerCase.substr(1);

  var context = {
    resource_name: resourceNameLowerCase,
    name_uppercase: nameFistCharUpperCase,
    alias_controller: resourceNameLowerCase + 'Ctrl'
  };

  return context;

}

/**
 * Válida se o nome do resource foi digitado corretamente
 * nos seguintes padrões: resource ou resource:subresource
 * @param {String} resourceName - Nome do recurso digitado
 * @return {boolean} Retorna um boolean para validação
 */
function validNameResource(resourceName) {
  var resources = resourceName.split(':');
  var regex = new RegExp('^[a-zA-Z]+:[a-zA-Z]+$');
  var error = false;

  if (resources.length > 1 && !regex.test(resourceName)) {
    console.log(chalk.red('Resource name invalid. Try again.'));
    error = true;
  }

  regex = new RegExp('^[a-zA-Z]+$');
  if (resources.length == 1 && !regex.test(resourceName)) {
    console.log(chalk.red('Resource name invalid. Try again.'));
    error = true;
  }

  return error;
}

/**
 * Desenha o logo no console
 * @return {String} retorna uma string personalizada da logo
 */
function logo() {
  var prodeb =
    chalk.blue.bold('                                             ,,,,,,;;;;;;;;;;;;:') + '\n' +
    chalk.blue.bold('                                                ´´´´´´;;;;;;;;;;') + '\n' +
    chalk.blue('  ++++++\'                                        ') + chalk.red.bold('`\'\'\'\'  ') + chalk.blue.bold(';;;;;;;') + '\n' +
    chalk.blue('  ++++++++                                          ') + chalk.red.bold(';\'\'; ') + chalk.blue.bold('::;;;;') + '\n' +
    chalk.blue('  ++\' `+++`                                          ') + chalk.red.bold('`\'\'; ') + chalk.blue.bold(';;;;') + '\n' +
    chalk.blue(' `++,  .++, +++++.    :+++.    ;+++.   ;;;;;;  ;;;;:   ') + chalk.red.bold('\'. ') + chalk.blue.bold(';;;') + '\n' +
    chalk.blue(' :++   ,++` ++++++.  ++++++   ++++++`  ++++++  ++++++  ') + chalk.red.bold('.\'\' ') + chalk.blue.bold(':;') + '\n' +
    chalk.blue(' +++   +++` ++  ++\'  ++  ++:  ++  ++\'  ++      ++  ++,  ') + chalk.red.bold('\'\' ') + chalk.blue.bold(':') + '\n' +
    chalk.blue(' +++;;+++; ,++  \'+: \'+;  ;+; `++  ,++  ++     .++  ++`  ') + chalk.red.bold('\'\'') + '\n' +
    chalk.blue(' +++++++;  \'+,  ++  ++   ++: ;+;  :+\' ,+++++  ++; .++   ') + chalk.red.bold(';') + '\n' +
    chalk.blue(' +++```    +++++\'   ++   ++. ++,  ++, ;+++++  +++++\'') + '\n' +
    chalk.blue(' ++:       ++++++;  ++   ++  ++   ++` ++++++  ++  \'++') + '\n' +
    chalk.blue(',++`       ++  +++  ++   ++  ++   ++  ++`     ++   ++') + '\n' +
    chalk.blue('\'++        ++  +++  ++  ,++  ++  ,++  ++      ++  ,++') + '\n' +
    chalk.blue('+++       ,++  ++:  ++,.++  `++:;++   ++++++. ++++++,') + '\n' +
    chalk.blue('+++       ;+\'  ++:   ++++    +++++   `++++++: +++++:') + '\n' +

    chalk.white.bgBlue('You are using the automatic angular component generator in a feature by folder strategy.');

  return prodeb;
}

/**
 * Valida se o nome do recurso foi digitado
 * @params {String, String} path, value - Caminho do arquivo e nome do arquivo
 * @return {boolean} Retorna true se o nome foi digitado
 */
function verifyInput(value) {
  if (value === undefined || value === '') {
    return chalk.red('The resource name is mandatory.');
  }
  return true;
}

module.exports = {
  normalizePath: normalizePath,
  formatNameResource: formatNameResource,
  verifyInput: verifyInput,
  logo: logo,
  validNameResource: validNameResource
}
