# C2Generator

> Gerador automatico da estrutura de arquivos

## Pré-requisitos

- [Nodejs](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/)

## Instalação

- Primeiro, instale o [Yeoman](http://yeoman.io)

```bash
npm install -g yo
```

- Depois, instale o Generator-C2Generator:

```bash
npm install -g git+ssh://git@git.prodeb.ba.gov.br:starter-pack/generator-ngprodeb.git
```

## Uso

- Execute o comando abaixo para iniciar o gerador de estrutura de arquivos:

```bash
yo c2generator
```

### Gerando os arquivos
 
* Para gerar sub-estruturas para o recurso digite o nome do recurso existente seguido
   de dois ponto e o nome da sub-estrutura , ex.:

```bash
yo ngprodeb

? Escolha qual estrutura a ser gerada? (Use arrow keys)
  Gerar estrutura completa. 
  Gerar estrutura html. 
❯ Gerar estrutura do controllerJS. 
  Gerar estrutura do serviceJS. 
  Gerar estrutura de routeJS. 
  Sair.

? Deseja criar os arquivos html correspondentes? (Use arrow keys)
❯ Sim 
  Não

? Digite o nome do recurso: projects:task
```

* Observação: Não é permitido nomes de recursos com caracteres especiais ou contendo números

## Problemas Comuns
 
### Erro de Permissão negada
  * Caso tente executar o comando yo ngprodeb e o seguinte erro for apresentado:

    ```bash
    /usr/lib/node_modules/yo/node_modules/configstore/index.js:53
                                    throw err;
                                    ^

    Error: EACCES: permission denied, open '/root/.config/configstore/insight-yo.json'
    You don't have access to this file.

        at Error (native)
        at Object.fs.openSync (fs.js:549:18)
        at Object.fs.readFileSync (fs.js:397:15)
        at Object.create.all.get (/usr/lib/node_modules/yo/node_modules/configstore/index.js:34:26)
        at Object.Configstore (/usr/lib/node_modules/yo/node_modules/configstore/index.js:27:44)
        at new Insight (/usr/lib/node_modules/yo/node_modules/insight/lib/index.js:37:34)
        at Object.<anonymous> (/usr/lib/node_modules/yo/lib/cli.js:172:11)
        at Module._compile (module.js:409:26)
        at Object.Module._extensions..js (module.js:416:10)
        at Module.load (module.js:343:32)
    ```

  * Execute o seguinte comando para corrigir o problema de permissão:

    ```bash
    chmod g+rwx /root /root/.config /root/.config/configstore
    ```

## Sobre Yeoman

 * Visite: [Yeoman.io](http://yeoman.io/).

## License

MIT © [Amon Caldas](codigocriativo.com)
